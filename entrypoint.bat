@echo off

setlocal EnableDelayedExpansion
	set ScriptPath=%~dp0
	set NL=^


:: Warning: Keep the amount of newlines above this line

	if defined MYSQL_ROOT_HOST set "MARIADB_ROOT_HOST=%MYSQL_ROOT_HOST%"
	if defined MYSQL_ROOT_PASSWORD set "MARIADB_ROOT_PASSWORD=%MYSQL_ROOT_PASSWORD%"
	if defined MYSQL_DATABASE set "MARIADB_DATABASE=%MYSQL_DATABASE%"
	if defined MYSQL_USER set "MARIADB_USER=%MYSQL_USER%"
	if defined MYSQL_PASSWORD set "MARIADB_PASSWORD=%MYSQL_PASSWORD%"

	if defined MYSQL_ROOT_HOST_FILE set "MARIADB_ROOT_HOST_FILE=%MYSQL_ROOT_HOST_FILE%"
	if defined MYSQL_ROOT_PASSWORD_FILE set "MARIADB_ROOT_PASSWORD_FILE=%MYSQL_ROOT_PASSWORD_FILE%"
	if defined MYSQL_DATABASE_FILE set "MARIADB_DATABASE_FILE=%MYSQL_DATABASE_FILE%"
	if defined MYSQL_USER_FILE set "MARIADB_USER_FILE=%MYSQL_USER_FILE%"
	if defined MYSQL_PASSWORD_FILE set "MARIADB_PASSWORD_FILE=%MYSQL_PASSWORD_FILE%"

	if defined MARIADB_ROOT_HOST_FILE (
		if defined MARIADB_ROOT_HOST_FILE (
			echo Both MARIADB_ROOT_HOST and MARIADB_ROOT_HOST_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_ROOT_HOST_FILE%" set /P MARIADB_ROOT_HOST=<"%MARIADB_ROOT_HOST_FILE%"
	)
	if not defined MARIADB_ROOT_HOST set "MARIADB_ROOT_HOST=%%"

	if defined MARIADB_ROOT_PASSWORD_FILE (
		if defined MARIADB_ROOT_PASSWORD (
			echo Both MARIADB_ROOT_PASSWORD and MARIADB_ROOT_PASSWORD_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_ROOT_PASSWORD_FILE%" set /p MARIADB_ROOT_PASSWORD=<"%MARIADB_ROOT_PASSWORD_FILE%"
	)

	if defined MARIADB_DATABASE_FILE (
		if defined MARIADB_DATABASE (
			echo Both MARIADB_DATABASE and MARIADB_DATABASE_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_DATABASE_FILE%" set /p MARIADB_DATABASE=<"%MARIADB_DATABASE_FILE%"
	)

	if defined MARIADB_USER_FILE (
		if defined MARIADB_USER (
			echo Both MARIADB_USER and MARIADB_USER_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_USER_FILE%" set /p MARIADB_USER=<"%MARIADB_USER_FILE%"
	)

	if defined MARIADB_PASSWORD_FILE (
		if defined MARIADB_PASSWORD (
			echo Both MARIADB_PASSWORD and MARIADB_PASSWORD_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_PASSWORD_FILE%" set /p MARIADB_PASSWORD=<"%MARIADB_PASSWORD_FILE%"
	)

	if defined MARIADB_ROOT_PASSWORD_HASH_FILE (
		if defined MARIADB_ROOT_PASSWORD_HASH (
			echo Both MARIADB_ROOT_PASSWORD_HASH and MARIADB_ROOT_PASSWORD_HASH_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_ROOT_PASSWORD_HASH_FILE%" set /p MARIADB_ROOT_PASSWORD_HASH=<"%MARIADB_ROOT_PASSWORD_HASH_FILE%"
	)

	if defined MARIADB_PASSWORD_HASH_FILE (
		if defined MARIADB_PASSWORD_HASH (
			echo Both MARIADB_PASSWORD_HASH and MARIADB_PASSWORD_HASH_FILE are set ^(but are exclusive^) 1>&2
			exit /B 1
		)

		if exist "%MARIADB_PASSWORD_HASH_FILE%" set /p MARIADB_PASSWORD_HASH=<"%MARIADB_PASSWORD_HASH_FILE%"
	)

	if defined MARIADB_ROOT_PASSWORD (
		if defined MARIADB_ROOT_PASSWORD_HASH (
			echo Both MARIADB_ROOT_PASSWORD and MARIADB_ROOT_PASSWORD_HASH are set ^(but are exclusive^) 1>&2
			exit /B 1
		)
	)

	if defined MARIADB_PASSWORD (
		if defined MARIADB_PASSWORD_HASH (
			echo Both MARIADB_PASSWORD and MARIADB_PASSWORD_HASH are set ^(but are exclusive^) 1>&2
			exit /B 1
		)
	)

	set "MariaDBPath=C:\mariadb"
	set "BinPath=%MariaDBPath%\bin"
	set "DataPath=%MariaDBPath%\data"
	set "InitPath=%MariaDBPath%\docker-entrypoint-initdb.d"
	set "ServerExe=mariadbd.exe"
	set "ClientExe=mariadb.exe"

	set "RunSQL=""%BinPath%\%ClientExe%"" --protocol=pipe --user=root --database=mysql"
	set "ExecuteSQL=%RunSQL% --execute"

	if not exist "%DataPath%\mysql\" (
		if defined MARIADB_ROOT_PASSWORD_HASH (
			"%BinPath%\mariadb-install-db.exe" --port=3306 --socket=MySQL --allow-remote-root-access
		) else (
			if not defined MARIADB_ROOT_PASSWORD (
				call :GeneratePassword "MARIADB_ROOT_PASSWORD"

				echo Root Password: !MARIADB_ROOT_PASSWORD!
			)

			"%BinPath%\mariadb-install-db.exe" --port=3306 --socket=MySQL --allow-remote-root-access --password=!MARIADB_ROOT_PASSWORD!
		)

		echo | set /p "NoNewLine=Starting temporary server"
		start "MariaDB-Temp" /B "%BinPath%\%ServerExe%" --skip-networking --enable-named-pipe --default-time-zone=SYSTEM --expire-logs-days=0 --loose-innodb_buffer_pool_load_at_startup=0
		call :WaitTempStart "ServerPID"

		if not defined MARIADB_ROOT_PASSWORD_HASH (
			echo Setting 'root' password for host 'localhost'...
			!ExecuteSQL! "ALTER USER 'root'@'localhost' IDENTIFIED BY '!MARIADB_ROOT_PASSWORD!';"

			set "MYSQL_PWD=!MARIADB_ROOT_PASSWORD!"
		)

		echo Creating 'RootRole'...
		!ExecuteSQL! "CREATE ROLE IF NOT EXISTS 'RootRole'; GRANT ALL ON *.* TO 'RootRole' WITH GRANT OPTION;"
		!ExecuteSQL! "GRANT 'RootRole' TO 'root'@'localhost' WITH ADMIN OPTION; SET DEFAULT ROLE 'RootRole';"
		rem ToDo: Uncomment once roles work correctly...
		rem !ExecuteSQL! "REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'root'@'localhost';"

		echo Creating root with host '!MARIADB_ROOT_HOST!'...
		if defined MARIADB_ROOT_PASSWORD_HASH (
			!ExecuteSQL! "CREATE OR REPLACE USER 'root'@'!MARIADB_ROOT_HOST!' IDENTIFIED BY PASSWORD '!MARIADB_ROOT_PASSWORD_HASH!';"
		) else (
			!ExecuteSQL! "CREATE OR REPLACE USER 'root'@'!MARIADB_ROOT_HOST!' IDENTIFIED BY '!MARIADB_ROOT_PASSWORD!';"
		)
		!ExecuteSQL! "GRANT 'RootRole' TO 'root'@'!MARIADB_ROOT_HOST!' WITH ADMIN OPTION; SET DEFAULT ROLE 'RootRole' FOR 'root'@'!MARIADB_ROOT_HOST!';"
		rem ToDo: Remove once roles work correctly...
		!ExecuteSQL! "GRANT ALL ON *.* TO 'root'@'!MARIADB_ROOT_HOST!' WITH GRANT OPTION;"

		echo Cleaning up outdated 'root' users...
		!ExecuteSQL! "DROP USER IF EXISTS 'root'@'127.0.0.1', 'root'@'::1';"
		!ExecuteSQL! "EXECUTE IMMEDIATE CONCAT( 'DROP USER IF EXISTS \'root\'@\'', @@hostname, '\'' );"

		echo Creating anonymous user with host 'localhost' ^(healthcheck^)...
		!ExecuteSQL! "CREATE USER IF NOT EXISTS ''@'localhost';"

		if defined MARIADB_DATABASE (
			echo Creating database "!MARIADB_DATABASE!"...
			!ExecuteSQL! "CREATE DATABASE IF NOT EXISTS `!MARIADB_DATABASE!`;"
		)

		if defined MARIADB_USER (
			if not defined MARIADB_ROOT_PASSWORD_HASH (
				if not defined MARIADB_PASSWORD (
					call :GeneratePassword "MARIADB_PASSWORD"

					echo User Password: !MARIADB_PASSWORD!
				)
			)

			echo Creating user "!MARIADB_USER!"...
			if defined MARIADB_PASSWORD_HASH (
				!ExecuteSQL! "CREATE USER IF NOT EXISTS '!MARIADB_USER!'@'%%' IDENTIFIED BY PASSWORD '!MARIADB_PASSWORD_HASH!';"
			) else (
				!ExecuteSQL! "CREATE USER IF NOT EXISTS '!MARIADB_USER!'@'%%' IDENTIFIED BY '!MARIADB_PASSWORD!';"
			)

			if defined MARIADB_DATABASE (
				echo Granting privileges to database "!MARIADB_DATABASE!" for user "!MARIADB_USER!"...
				!ExecuteSQL! "GRANT ALL ON `!MARIADB_DATABASE!`.* TO '!MARIADB_USER!'@'%%';"

				if exist "%InitPath%\*.sql" (
					echo Initializing Database using %InitPath%\*.sql...
					for /F "tokens=*" %%F in ( 'dir /B /s %InitPath%\*.sql' ) do !RunSQL! --user=!MARIADB_USER! --database=!MARIADB_DATABASE! < %%F
				)
			)
		)


		if defined MARIADB_ROOT_PASSWORD_HASH (
			echo Setting 'root' password for host 'localhost'...
			!ExecuteSQL! "ALTER USER 'root'@'localhost' IDENTIFIED BY PASSWORD '!MARIADB_ROOT_PASSWORD_HASH!';"
		) else (
			set "MYSQL_PWD="
		)

		if defined ServerPID (
			echo | set /p "NoNewLine=Stopping temporary server"
			taskkill /PID !ServerPID! /T /F 1>NUL
			call :WaitTempStop !ServerPID!

			set "ServerPID="
		)
	)

	echo Starting server...
	"%BinPath%\%ServerExe%" --enable-named-pipe %*

:End
endlocal
exit /B

:WaitTempStart
	set "%~1="

	for /L %%i in ( 1, 1, 50 ) do (
		!ExecuteSQL! "SELECT 1;" 1>NUL 2>&1
		if !ERRORLEVEL! EQU 0 (
			echo;

			for /F "TOKENS=1,2" %%O in ( 'tasklist /NH /FI "ImageName eq %ServerExe%"' ) do (
				if "%%O" == "%ServerExe%" set "%~1=%%P"
			)

			exit /B
		) else (
			if !ERRORLEVEL! NEQ 0 (
				echo | set /p "NoNewLine=."

				ping 192.0.2.1 -n 1 -w 1000 1>NUL 2>&1
			)
		)
	)
exit /B

:WaitTempStop
setlocal EnableDelayedExpansion
	set "ServerPID=%~1"

	for /L %%i in ( 1, 1, 50 ) do (
		tasklist /NH /FI "PID eq %ServerPID%" | findstr "%ServerExe%" 1>NUL 2>&1
		if !ERRORLEVEL! EQU 0 (
			echo | set /p "NoNewLine=."

			ping 192.0.2.1 -n 1 -w 1000 1>NUL 2>&1
		) else (
			if !ERRORLEVEL! NEQ 0 (
				echo;

				endlocal
				exit /B
			)
		)
	)
endlocal
exit /B

:GeneratePassword
setlocal EnableDelayedExpansion
	set "Password="

	set "PassChars=aabccdeeefghiijklmnopqrstuuuvwxyzAABCCDEEEFGHIIJKLMNOPQRSTUUUVWXYZ11223344556677889900__++--**&&§§$$??()"
	set "PassCharCount=104"

	for /L %%i in ( 1, 1, 60 ) do (
		set /a iChar=!random! %% PassCharCount
		call set "Password=!Password!%%PassChars:~!iChar!,1%%"
	)
endlocal & set "%~1=%Password%"
exit /B
