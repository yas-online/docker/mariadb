# escape=`

# ToDo: Investigate if NanoServer really can't be used...
# ERROR : Data directory C:\mariadb\data is not empty. Only new or empty existing directories are accepted for --datadir
#ARG WINDOWS_OS=nanoserver
ARG WINDOWS_OS=servercore
ARG WINDOWS_VERSION=2004

ARG MARIADB_VERSION=11.3.2

FROM mcr.microsoft.com/windows/${WINDOWS_OS}:${WINDOWS_VERSION} AS image

ARG MARIADB_VERSION

LABEL windows.image.name=${WINDOWS_OS}
LABEL windows.image.version=${WINDOWS_VERSION}
LABEL org.mariadb.version=${MARIADB_VERSION}

SHELL ["cmd", "/S", "/C"]
WORKDIR c:/mariadb

EXPOSE 3306/tcp

HEALTHCHECK CMD healthcheck.bat

ENTRYPOINT ["entrypoint.bat"]

RUN `
	curl --location --output mariadb.zip https://downloads.mariadb.org/rest-api/mariadb/%MARIADB_VERSION%/mariadb-%MARIADB_VERSION%-winx64.zip && `
	tar -xf mariadb.zip && `
	xcopy /E /H /C /I /Q mariadb-%MARIADB_VERSION%-winx64 && `
	rmdir /S /Q mariadb-%MARIADB_VERSION%-winx64 && `
	del mariadb.zip

RUN `
	mkdir data && `
	mkdir docker-entrypoint-initdb.d && `
	for /F "usebackq tokens=2*" %O in ( `reg query "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Environment" /v PATH` ) do set "SystemPath=%P" && `
	call setx /M PATH "%SystemPath%";c:\mariadb\;c:\mariadb\bin\

# ToDo: find a way to use volumes without admin privileges
#USER ContainerUser
VOLUME c:/mariadb/data

COPY ["*.bat", "./"]

CMD [ "--console" ]
