@echo off

setlocal EnableDelayedExpansion
	set ScriptPath=%~dp0
	set NL=^


:: Warning: Keep the amount of newlines above this line

	set "MariaDBPath=C:\mariadb"
	set "BinPath=%MariaDBPath%\bin"
	set "DataPath=%MariaDBPath%\data"
	set "InitPath=%MariaDBPath%\docker-entrypoint-initdb.d"
	set "ServerExe=mariadbd.exe"
	set "ClientExe=mariadb.exe"

	set "RunSQL=""%BinPath%\%ClientExe%"" --protocol=pipe"
	set "ExecuteSQL=%RunSQL% --execute"

	%ExecuteSQL% "SELECT 1;" 1>NUL 2>&1
	if !ERRORLEVEL! EQU 0 (
		%RunSQL% --skip-column-names --execute "SELECT 1 FROM `information_schema`.`ENGINES` WHERE `ENGINE`='InnoDB' AND `SUPPORT` in ( 'YES', 'DEFAULT', 'ENABLED' );" 1>NUL 2>&1
		exit /B !ERRORLEVEL!
	) else (
		exit /B 1
	)

:End
endlocal
exit /B
